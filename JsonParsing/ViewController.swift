//
//  ViewController.swift
//  JsonParsing
//
//  Created by Ezaden Seraj on 26/08/2017.
//  Copyright © 2017 Ezaden Seraj. All rights reserved.
//

import UIKit

struct WebsiteDescription: Decodable {
    let name : String
    let description : String
    let courses : [Course]
}

struct Course: Decodable {
    let id : Int
    let name : String
    let link : String
    let imageUrl : String
    
//    init(json: [String : Any]) {
//        id = json["id"] as? Int ?? -1
//        name = json["name"] as? String ?? ""
//        link = json["link"] as? String ?? ""
//        imageUrl = json["imageUrl"] as? String ?? ""
//
//    }
    
}

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    
        parseJson()
        doPost()
        
    }

    //MARK:- Traditional way of JSON Parsing
    func parseJson() {
        let jsonUrlString = "https://api.letsbuildthatapp.com/jsondecodable/website_description"
        
        guard let url = URL(string: jsonUrlString) else {return}
        
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            
            if let response = response {
                print(response)
            }
            
            guard let data = data else {return}
            
                do {
                    
                    let websiteDescription = try JSONDecoder().decode(WebsiteDescription.self, from: data)
                    print(websiteDescription.name)
//                    let courses = try JSONDecoder().decode([Course].self, from: data)
//
//                    print(courses)
                    
                } catch {
                    print(error)
                }
            
            
            
        }.resume()
        
        
        
//        URLSession.shared.dataTask(with: url) { (data, response, error) in
//
//            guard let data = data else {return}
//
////            let dataAsString = String(data: data, encoding: .utf8)
//
//
//            do {
//
//               guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] else {return}
//
//                let course = Course(json: json)
//
//                print(course.name)
//
//
//            } catch let jsonError {
//                print("json serializining", jsonError)
//            }
//
//        }.resume()
    }
    
    func doPost() {
        
        let parameters = ["username": "ezuu", "tweet": "something"]
        let urlString = "https://jsonplaceholder.typicode.com/posts"
        guard let url = URL(string: urlString) else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else {return}
        request.httpBody = httpBody
        
        let session = URLSession.shared
        
        session.dataTask(with: request) { (data, response, error) in
            
            if let response = response {
                print(response)
            }
            
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                } catch {
                    print(error)
                }
            }
            
        }.resume()
        
    }


}

